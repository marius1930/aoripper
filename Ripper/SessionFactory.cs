﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AORipper.Ripper {
    public class SessionFactory {
        private static ISessionFactory _sessionFactory;
        public static bool ExportSchemas { get; set; }
        public static bool DropSchemas { get; set; }

        public static string ConnectionString {
            get;
            set;
        }

        public static bool ForceSQLite { get; set; }
        public static bool ForceMySQL { get; set; }

        private static ISessionFactory sessionFactory {
            get {
                if (_sessionFactory == null) {
                    var configuration = new Configuration();
                    configuration.Configure();
                    configuration.AddAssembly(Assembly.GetExecutingAssembly());

                    // If we're creating schemas, make sure we drop them first 'if exists'
                    if (DropSchemas || ExportSchemas)
                        new SchemaExport(configuration).Execute(true, true, true);

                    if (ExportSchemas)
                        new SchemaExport(configuration).Execute(true, true, false);

                    // Override connection string only if required
                    if (!string.IsNullOrEmpty(ConnectionString))
                        configuration.SetProperty("connection.connection_string", ConnectionString);

                    if (ForceSQLite) {
                        configuration.SetProperty("dialect", "NHibernate.Dialect.SQLiteDialect");
                        configuration.SetProperty("connection.driver_class", "NHibernate.Driver.SQLite20Driver");
                    }
                    else if (ForceMySQL) {
                        configuration.SetProperty("dialect", "NHibernate.Dialect.MySQL55Dialect");
                        configuration.SetProperty("connection.driver_class", "NHibernate.Driver.MySqlDataDriver");
                    }
                    
                    _sessionFactory = configuration.BuildSessionFactory();
                }
                return _sessionFactory;
            }
        }

        public static ISession OpenSession() {
            return sessionFactory.OpenSession();
        }
        public static IStatelessSession OpenStatelessSession() {
            return sessionFactory.OpenStatelessSession();
        }
    }
}
