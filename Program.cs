﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using AORipper.Structures;
using System.IO;
using AORipper.Ripper;
using NHibernate;
using NDesk.Options;
using System.Text.RegularExpressions;

namespace AORipper {
    class Program {
        static ILog logger = LogManager.GetLogger(typeof(Program));
        static long numSaved = 0;
        static long numSavedSinceReconnect = 0;
        static void Save(List<AOItem> items) {
            using (ISession session = SessionFactory.OpenSession()) {
                using (ITransaction transaction = session.BeginTransaction()) {
                    foreach (AOItem item in items) {
                        foreach (var m in item.reqs)
                            m.item = item;
                        session.Save(item);
                    }
                    transaction.Commit();

                    numSaved += items.Count;
                    numSavedSinceReconnect += items.Count;

                    logger.DebugFormat("Saved {0} items ({1} total)", items.Count, numSaved);
                }
            }
        }


        private static void PrintHelp() {
            Console.Write(@"
Extracts item data from Anarchy Online and stores them to a database.
The output database is specified in hibernate.cfg.xml

AORIPPER [/i] [/n] [/dt] [/ct] source

  source        Specifies the location of Anarchy Online
  /i            Indicates that item data should be extracted
  /n            Indicates that nano data should be extracted
  /ct           Creates the necessary tables on target database (runs /dt first)
  /dt           Drops the relevant tables on target database
");/*
  Overrides for hibernate.cfg.xml
  Setting these switches will override the pre-configured output settings
  /o=?          Set the SQLite output file
  /host=?       Set MySQL host
  /port=?       Set MySQL port
  /user=?       Set MySQL username
  /pass=?       Set MySQL password
  /db=?         Set MySQL database
");*/
        }

        class CommandLineOptions {
            public bool ExtractItems { get; set; }
            public bool ExtractNanos { get; set; }
            public string Path { get; set; }
        }

        /// <summary>
        /// Attempt to extract the path from input parameters
        /// This is primarily to catch unquoted paths with whitespace
        /// </summary>
        /// <param name="extra"></param>
        /// <returns></returns>
        private static string GetPathFromExtras(List<string> extra) {
            if (extra.Count == 0)
                return string.Empty;

            else if (extra.Count == 1)
                return extra[0];

            else {
                if (Directory.Exists(extra[0]))
                    return extra[0];
                
                var alt = string.Join(" ", extra);
                if (Directory.Exists(alt))
                    return alt;
            }

            return string.Empty;
        }


        /// <summary>
        /// Parse input parameters
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static CommandLineOptions ParseOptions(string[] args) {
            string sqliteOutput = null;
            string mysqlHostname= null;
            string mysqlPort = null;
            string mysqlUsername = null;
            string mysqlPassword = null;
            string mysqlDatabase = null;

            CommandLineOptions result = new CommandLineOptions();
            bool help   = false;      
            var p = new OptionSet () {
                { "n|nanos", v => result.ExtractNanos = v != null },
                { "i|items", v => result.ExtractItems = v != null },
                { "ct", v => SessionFactory.ExportSchemas = v != null },
                { "dt", v => SessionFactory.DropSchemas = v != null },
                /*{ "o=", v => sqliteOutput = v },
                { "host=", v => mysqlHostname = v },
                { "port=", v => mysqlPort = v },
                { "user=", v => mysqlUsername = v },
                { "pass=", v => mysqlPassword = v },
                { "db=", v => mysqlDatabase = v },*/
                { "h|?|help", v => help = v != null },
            };
            List<string> extra = p.Parse (args);
            result.Path = GetPathFromExtras(extra);

            if (help || extra.Count == 0) {
                PrintHelp();
            }
            else if (!Directory.Exists(result.Path)) {
                Console.WriteLine(string.Format("Could not find the folder \"{0}\"", result.Path));
                Console.WriteLine();
            }
            else if (!File.Exists(string.Format("{0}/cd_image/data/db/ResourceDatabase.idx", result.Path))) {
                Console.WriteLine(string.Format("Could not find Anarchy Online in the folder \"{0}\"", result.Path));
                Console.WriteLine();
            }
            else if (!(result.ExtractItems || result.ExtractNanos)) {
                Console.WriteLine("No data to extract, neither -i nor -n provided.");
                Console.WriteLine();
            }
            else {
                /*
                SessionFactory.ForceSQLite = !string.IsNullOrEmpty(sqliteOutput);
                SessionFactory.ForceMySQL = !string.IsNullOrEmpty(mysqlHostname) || !string.IsNullOrEmpty(mysqlPort) 
                    || !string.IsNullOrEmpty(mysqlUsername) || !string.IsNullOrEmpty(mysqlPassword) || !string.IsNullOrEmpty(mysqlDatabase);

                if (SessionFactory.ForceMySQL && SessionFactory.ForceSQLite) {
                    Console.WriteLine("Invalid arguments: Both SQLite and MySQL arguments has been provided");
                    Console.WriteLine("  Please provide override for one database only, alternately configure hibernate.cfg.xml");
                    Console.WriteLine();
                    return null;
                }

                if (!string.IsNullOrEmpty(sqliteOutput)) {
                    SessionFactory.ConnectionString = string.Format("Data Source = {0};Version=3", sqliteOutput);
                }
                // Load the default values and override only the specified ones
                else if (SessionFactory.ForceMySQL) {
                    MysqlOverrides defaults = GetConfigurationDefaults();

                    if (!string.IsNullOrEmpty(mysqlHostname))
                        defaults.Host = mysqlHostname;

                    if (!string.IsNullOrEmpty(mysqlPort))
                        defaults.Port = mysqlPort;

                    if (!string.IsNullOrEmpty(mysqlUsername))
                        defaults.Username = mysqlUsername;

                    if (!string.IsNullOrEmpty(mysqlPassword))
                        defaults.Password = mysqlPassword;

                    if (!string.IsNullOrEmpty(mysqlDatabase))
                        defaults.Database = mysqlDatabase;

                    SessionFactory.ConnectionString = defaults.ConnectionString;
                }

                // Attempt to connect and verify that the tables exists
                if (!string.IsNullOrEmpty(SessionFactory.ConnectionString)) {
                    try {
                        using (ISession session = SessionFactory.OpenSession()) {
                            //session.CreateSQLQuery("SELECT * FROM aoripper_item LIMIT 1").List();
                        }
                    }
                    catch (Exception ex) {
                        logger.Warn(ex.Message);
                        logger.Info(SessionFactory.ConnectionString);
                        return null;
                    }
                }*/

                return result;
            }

            return null;
        }


        class MysqlOverrides {
            public string Host { get; set; }
            public string Port { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
            public string Database { get; set; }
            public string ConnectionString {
                get {
                    return string.Format("Data Source={0};Port={1};Database={2};User Id={3};Password={4}", Host, Port, Database, Username, Password);
                }
            }
        }


        /// <summary>
        /// Load default values from hibernate.cfg.xml
        /// Mainly aimed at MySQL, but should work for most databases
        /// </summary>
        /// <returns></returns>
        static MysqlOverrides GetConfigurationDefaults() {
            MysqlOverrides defaults = new MysqlOverrides();
            string data = File.ReadAllText("hibernate.cfg.xml");
            {
                Match match = Regex.Match(data, @".*port\s*=\s*(\d+).*", RegexOptions.IgnoreCase);
                if (match.Success)
                    defaults.Port = match.Groups[1].Value;
            }
            {
                Match match = Regex.Match(data, @".*User Id\s*=\s*([^;<\r\n]+).*", RegexOptions.IgnoreCase);
                if (match.Success)
                    defaults.Username = match.Groups[1].Value;
            }
            {
                Match match = Regex.Match(data, @".*Password\s*=\s*([^;<\r\n]+).*", RegexOptions.IgnoreCase);
                if (match.Success)
                    defaults.Password = match.Groups[1].Value;
            }
            {
                Match match = Regex.Match(data, @".*Data Source\s*=\s*([^;<\r\n]+).*", RegexOptions.IgnoreCase);
                if (match.Success)
                    defaults.Host = match.Groups[1].Value;
            }
            {
                Match match = Regex.Match(data, @".*database\s*=\s*([^;<\r\n]+).*", RegexOptions.IgnoreCase);
                if (match.Success)
                    defaults.Database = match.Groups[1].Value;
            }

            return defaults;
        }

        /// <summary>
        /// Parse input and pass it along to Extract, if validation succeeds.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args) {
            logger.Info("Starting AORipper..");
            CommandLineOptions options;
            try {
                options = ParseOptions(args);
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return;
            }

            if (options != null) {
                List<ResourceType> types = new List<ResourceType>();

                if (options.ExtractItems)
                    types.Add(ResourceType.AODB_TYP_ITEM);

                if (options.ExtractNanos)
                    types.Add(ResourceType.AODB_TYP_NANO);

                Extract(options.Path, types);
            }

        }


        /// <summary>
        /// Extract data from the resource files
        /// </summary>
        /// <param name="path"></param>
        /// <param name="resources"></param>
        static void Extract(string path, List<ResourceType> resources) {
            string indexFile = string.Format("{0}/cd_image/data/db/ResourceDatabase.idx", path);
            Indexer s = new Indexer(indexFile, resources);

            Indexer s0 = new Indexer(string.Format("{0}/cd_image/data/db/ResourceDatabase.idx", @"E:\Games\Anarchy Online"), resources);
            Indexer s1 = new Indexer(string.Format("{0}/cd_image/data/db/ResourceDatabase.idx", @"E:\Games\Anarchy Online-OLD"), resources);
            var n0 = s0.GetOffsets(ResourceType.AODB_TYP_ITEM);
            var n1 = s1.GetOffsets(ResourceType.AODB_TYP_ITEM);

            // Delete any data already in the tables
            try {
                using (ISession session = SessionFactory.OpenSession()) {
                    using (ITransaction transaction = session.BeginTransaction()) {
                        session.CreateQuery("DELETE FROM AOItem").ExecuteUpdate();
                        session.CreateSQLQuery("DELETE FROM aoripper_item_attribs").ExecuteUpdate();
                        session.CreateSQLQuery("DELETE FROM aoripper_item_def").ExecuteUpdate();
                        session.CreateSQLQuery("DELETE FROM aoripper_item_other").ExecuteUpdate();
                        session.CreateQuery("DELETE FROM AOItemRequirements").ExecuteUpdate();

                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex) {
                logger.Warn(ex.Message);
                logger.Warn(ex.StackTrace);
                return;
            }

            foreach (var type in resources) {
                SortedSet<uint> offsets = s.GetOffsets(type);

                int itemindex = 0;
                List<AOItem> items = new List<AOItem>();
                Parser parser = new Parser(Parser.GetResourceFiles(path));
                logger.InfoFormat("Detected {0} objects of type {1}", offsets.Count, type);
                foreach (uint offset in offsets) {


                    AOItem item = parser.GetItem(offset);
                    if (item != null)
                        items.Add(item);

                    itemindex++;

                    if (items.Count >= 256) {
                        Save(items);
                        items = new List<AOItem>();
                    }
                }


                foreach (var item in items) {
                    Console.WriteLine(item.name);
                    Console.WriteLine(item.description);
                    foreach (var effect in item.effects) {
                        Console.WriteLine(string.Format("{0}: {1}", effect.Label, effect.Value));
                    }
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("==========================");
                    Console.WriteLine();

                    if (item.aoid == 303061) {
                        int y = 9;
                    }
                }
                
                if (items.Count > 0)
                    Save(items);


            }
        }
    }
}
