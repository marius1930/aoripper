BEGIN TRANSACTION;
CREATE TABLE "aoripper_item_requirements" (
	`id`	INTEGER NOT NULL,
	`type`	INTEGER,
	`attribute`	INTEGER,
	`count`	INTEGER,
	`op`	INTEGER,
	`opmod`	NUMERIC,
	`aoid`	INTEGER,
	`pk`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE
);
CREATE TABLE `aoripper_item_other` (
	`aoid`	INTEGER NOT NULL,
	`key`	INTEGER NOT NULL,
	`val`	INTEGER NOT NULL,
	PRIMARY KEY(aoid,key)
);
CREATE TABLE `aoripper_item_def` (
	`aoid`	INTEGER NOT NULL,
	`key`	INTEGER NOT NULL,
	`val`	INTEGER NOT NULL,
	PRIMARY KEY(aoid,key)
);
CREATE TABLE `aoripper_item_attribs` (
	`aoid`	INTEGER NOT NULL,
	`key`	INTEGER NOT NULL,
	`val`	INTEGER NOT NULL,
	PRIMARY KEY(aoid, key)
);
CREATE TABLE "aoripper_item" (
	`aoid`	bigint NOT NULL,
	`flags`	integer,
	`props`	integer,
	`ql`	integer,
	`icon`	integer,
	`name`	charactervarying(1024),
	`description`	text,
	`type`	integer,
	`slot`	integer,
	`defslot`	integer,
	`value`	integer,
	`tequip`	integer,
	`crystal`	integer,
	`ncu`	integer,
	`nanocost`	integer,
	`school`	integer,
	`strain`	integer,
	`duration`	integer,
	`multim`	integer,
	`multir`	integer,
	`tburst`	integer,
	`tfullauto`	integer,
	`clip`	integer,
	`dcrit`	integer,
	`atype`	integer,
	`mareq`	integer,
	`tattack`	integer,
	`trecharge`	integer,
	`dmax`	integer,
	`dmin`	integer,
	`_range`	integer,
	`dtype`	integer,
	`initskill`	integer
);
COMMIT;
