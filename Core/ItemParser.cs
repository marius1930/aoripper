﻿using AORipper.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace AORipper {
    class ItemParser {
        static ILog logger = LogManager.GetLogger(typeof(ItemParser));
        private int offset;
        private byte[] data;

        private long Remaining {
            get {
                return data.Length - offset;
            }
        }

        #region CONSTANTS
        const int UPLOAD_NANO = 0x1B;
        const int TEXT_EFFECT = 0x34;
        const int UNKNOWN_TEXT1 = 0x3E;
        const int TEXT_FLOAT = 0x41;
        const int UNKNOWN_TEXT2 = 0x70;
        const int UNKNOWN_TEXT3 = 0x7B;
        const int UNKNOWN_TEXTS1 = 0x8C;
        const int UNKNOWN_TEXT4 = 0x8E;
        const int UNKNOWN_TEXT5 = 0xDA;
        const int DEBUG_TEXT = 0xE5;
        const int EVENT_TEXT = 0xFC;
        const int UNKNOWN_TEXT_N = 0xFF;
        #endregion

        #region EFFECTS
        static Dictionary<ushort, UInt16> s_effectkeys = new Dictionary<ushort, UInt16>() {
            { 0x0a,   4}, // AO: Random modify [attribute, min, max, ac]
            { 0x0b,   5}, // ??: SL: unknown, shadowlands, unsure of size or purpose
            { 0x14,   3}, // AO: set attribute [attribute, value]
            { 0x16,   5}, // AO: temporary skill modify [skill, modifier, ?, ?, time]
            { 0x18,   4}, // AO: teleport [x,y,z,playfield]
            { 0x1b,   0}, // AO: upload nano [id] (we cheat here (count=0), the value is gotten by pre-effect check)
            { 0x21,   3}, // ??: SL: unknown, unsure of size or purpose
            { 0x22,   3}, // AO: set value [?, key, value]
            { 0x24,   3}, // AO: add skill [value, key]
            { 0x26,   7}, // AO: some graphics effect crap, 7 bytes maybe
            { 0x28,   4}, // AO: save character.. unknown values (4?)
            { 0x29,   3}, // AO: lock skill ?, key, value
            { 0x2b,   2}, // AO: headmesh, unknown, meshid
            { 0x2d,   2}, // AO: backmesh, unknown, meshid
            { 0x2e,   2}, // ??: unknown, 2 bytes likely
            { 0x2f,   3}, // AO: texture, texid, slot?, unknown
            { 0x34,   0}, // AO: text (chat) length, text
            { 0x35,   2}, // AO: modify skill, key, value
            { 0x3b,   1}, // AO: execute nano (self?), id
            { 0x3e,   0}, // AO: text (unknown) length, text
            { 0x3f,   2}, // AO: attractor mesh
            { 0x41,   0}, // AO: text (floating) length, text
            { 0x44,   2}, // AO: change shape, ? ?
            { 0x47,  10}, // AO: summon mob, ? ? ? ?
            { 0x48,   8}, // AO: summon item, ? ? ? ?
            { 0x49,   4}, // ??: unknown, 4 bytes
            { 0x4a,   1}, // AO: execute nano (team?), id
            { 0x4b,   2}, // AO: change status
            { 0x4c,   2}, // AO: restrict, ? ?
            { 0x4d,   0}, // AO: next head model
            { 0x4e,   0}, // AO: previous head model
            { 0x51,   5}, // AO: area effect, key, min, max, type, range
            { 0x53,   4}, // ??: unknown, 4 bytes most likely
            { 0x54,   4}, // ??: unknown, 4 bytes
            { 0x56,   1}, // ??: SL: unknown, unsure of size or purpose
            { 0x57,   3}, // AO: change vision, ? ? ?
            { 0x5a,   6}, // ??: SL: unknown, unsure of size or purpose
            { 0x5b,   7}, // AO: teleport, ? playfield ? ? ?
            { 0x5e,   0}, // AO: refresh model
            { 0x5f,   2}, // AO: area nano, id, range
            { 0x61,   2}, // AO: cast nano, id, chance to cast
            { 0x64,   0}, // AO: open bank
            { 0x6c,   1}, // AO: equip monster weapon, 4byteid
            { 0x70,	  0}, // ??: Unknown text string
            { 0x71,   3}, // AO: remove nanos under x ncu [ncumax, school, count]
            { 0x73,   1}, // AO: script [id]
            { 0x75,   0}, // AO: Create OR enter apartment
            { 0x76,   2}, // AO: set value key, value
            { 0x7b,   0}, // AO: text (unknown) length, text
            { 0x7d,   5}, // AO: taunt, 0,0,taunt value
            { 0x7e,   0}, // AO: pacify
            { 0x81,   0}, // AO: fear
            { 0x82,   0}, // ??: unknown, zero.. in 12.80 nano data
            { 0x84,   9}, // AO: random spawn item, [chance, id, ql] 9 bytes likely
            { 0x86,   0}, // AO: wipe hate list
            { 0x87,   0}, // AO: charm
            { 0x88,   0}, // AO: daze, zero arguments, only in 'daze spell test'
            { 0x8a,   0}, // AO: Destroy item
            { 0x8c,   0}, // AO: text (dual) len, text, len, text
            { 0x8d,   1}, // AO: Organization type .. 1 byte MAYBE
            { 0x8e,   0}, // AO: text (??) 0, len, text
            { 0x91,   1}, // AO: Create OR enter apartment .. 1 byte MAYBE
            { 0x92,   0}, // AO: zero content, enable flight
            { 0x93,   2}, // AO: Set flag, attribute, bit
            { 0x94,   2}, // AO: Enable feature.. name change gate has it
            { 0x96,   4}, // ??: unknown, could be 4 bytes, or less.. at least 2. present in 11.00 data
            { 0x98,   0}, // AO: warp to last save
            { 0xa1,   0}, // ??: unknown
            { 0xa2,   0}, // AO: summon selected player
            { 0xa3,   0}, // AO: summon team members
            { 0xaa,   2}, // AO: Resistance to nano [strain, percentage]
            { 0xac,   0}, // SL: Save character
            { 0xae,   0}, // ??: unknown, 0, only name giver has it
            { 0xaf,  16}, // AO: Spawn pet, 4byteid, ?, ?
            { 0xb5,   0}, // ??: unknown, zero content
            { 0xb7,   2}, // NW: Org advantage
            { 0xb9,   2}, // AO: Reduce nano strain length, strain, seconds
            { 0xba,   0}, // NW: Shield disabler, notum wars shite, zero content
            { 0xbd,   1}, // AO: Warp pets to user
            { 0xbe,   4}, // SL: Add action
            { 0xc0,   2}, // SL: Modify attribute by percentage
            { 0xc1,   5}, // SL: "Drain hit", attribute, minimum, maximum, armor-attribute, recover %
            { 0xc3,   3}, // SL: Lock perk
            { 0xc5,   2}, // 18.5.3: Set Faction - AOID 297262
            { 0xc7,   1}, // ??: "Monster Sit useitem" - AOID 283566
            { 0xc8,  11}, // 
            { 0xc9,   2}, // ??: SL: unknown
            { 0xcc,   4}, // SL: "Special hit", attribute, minimum, maximum, armor-attribute
            { 0xce,   2}, // 18.5.0 : ?? - AOID 287559 
            { 0xd4,  13}, // ??: SL: unknown
            { 0xd6,   1}, // ??: SL: unknown
            { 0xd8,   0}, // SL: Set anchor
            { 0xd9,   0}, // SL: Recall to anchor
            { 0xda,   0}, // ??: SL: 15.0.6-ep1 text
            { 0xdd,   8}, // ??: ControlHate-Self <unknown... x8>
            { 0xe4,   9}, // 
            { 0xe5,   0}, // 
            { 0xe6,   4}, // 
            { 0xe7,   4}, // 
            { 0xe8,   2}, // ??: AddDefProc-Self <%chance, AOID>
            { 0xe9,   0}, // ??: AOID = 283789
            { 0xea,   3}, // ??: SpawnQuest-Self <quest-id, unknown, unknown>
            { 0xeb,   2}, //
            { 0xec,   1}, // ??: PlayfieldNano-Self <AOID>
            { 0xed,   1}, // ??: SolveQuest-Self <quest-id>
            { 0xee,   3}, // ??: KnockBack-Self <unknown, unknown, unknown> / KnockBack-Target?
            { 0xef,   0}, // 17.10.0: ??
            { 0xf0,   0}, // 18.0.0 : ?? AOID = 278587
            { 0xf1,   6}, // 18.1.0 : ?? Instanced City gate?
            { 0xf2,   0}, // 18.0.1 : ?? 
            { 0xf3,   3}, // 18.1.0 : ?? Instanced City Guest Key Generator
            { 0xf4,   1}, // 18.0.0 : ?? AOID = 280162
            { 0xf5,   2}, // 18.5.0 : ?? AOID = 287559
            { 0xf6,   2}, // 18.5.0 : ?? AOID = 296265
            { 0xf7,   1}, // 18.5.x : Change Gender - AOID = 296265
            { 0xf8,	 1}, // 18.3.x : ?? AOID = 202260
            { 0xfa,   1}, // 18.4.6 : CastNano? AOID = 290265
            { 0xfc,   0}, // Text string
            { 0xff,   0} // 18.5.0 : string + number - AOID = 292479
        };
        #endregion

        #region HelperMethods
        uint PopUInteger() {
            uint v = BitConverter.ToUInt32(data, offset);
            offset += 4;
            return v;
        }

        ushort PopUShort() {
            ushort v = BitConverter.ToUInt16(data, offset);
            offset += 2;
            return v;
        }

        int PopSInteger() {
            int v = BitConverter.ToInt32(data, offset);
            offset += 4;
            return v;
        }

        bool CanPopString(int length) {
            if (length > data.Length - offset || length > 1024 * 10 || length < 0) {
                return false;
            }
            return true;
        }

        string PopString(int length) {
            if (length > data.Length - offset || length > 1024 * 10 || length < 0) {
                offset = data.Length;
                throw new ArgumentOutOfRangeException(String.Format("Could not parse string of length {0} in block of length {1} at offset {2}", length, data.Length, offset));
            }

            char[] sub = new char[length];
            for (int i = 0; i < length; i++) {
                sub[i] = Convert.ToChar(data[offset + i]);
            }

            string retval = new String(sub);
            offset += length;
            return retval;
        }
        #endregion


        public AOItem Parse(byte[] data, int size) {
            AOItem item = new AOItem();
            item.aoid = BitConverter.ToUInt32(data, 0);
            this.data = data;

            /*
            if (item.aoid == 248916) {
                int x = 9;
            }
            else {
                return null;
            }*/

            offset = 0x20;

            bool head = true;
            int ftype = 0;

            while (offset + 4 < data.Length) {
                uint key;
                int val;

                #region HEAD
                if (head) {
                    key = PopUInteger();
                    val = PopSInteger();

                    switch (key) {

                        case 0x00:
                            item.flags = val;
                            break;

                        case 0x08:
                            item.duration = val;
                            break;

                        case 0x17:
                            break;

                        case 0x1e:
                            item.props = val;
                            break;

                        case 0x36:
                            item.ql = val;
                            break;

                        case 0x4a:
                            item.value = val;
                            break;

                        case 0x4b:
                            item.strain = val;
                            break;

                        case 0x4c:
                            item.type = val;
                            break;

                        case 0x4f:
                            item.icon = val;
                            break;

                        case 0x58:
                            item.defslot = val;
                            break;

                        case 0x64:
                            item.mareq = val;
                            item.other[(int)key] = val;
                            break;

                        case 0x65:
                            item.multim = val;
                            item.other[(int)key] = val;
                            break;

                        case 0x86:
                            item.multir = val;
                            item.other[(int)key] = val;
                            break;

                        case 0xd2:
                            item.trecharge = val;
                            break;

                        case 0xd3:
                            item.tequip = val;
                            break;

                        case 0xd4:
                            item.clip = val;
                            break;

                        case 0x11c:
                            item.dcrit = val;
                            break;

                        case 0x11d:
                            item.dmax = val;
                            break;

                        case 0x11e:
                            item.dmin = val;
                            break;

                        case 0x11f:
                            item.range = val;
                            break;

                        case 0x126:
                            item.tattack = val;
                            break;

                        case 0x12a:
                            item.slot = val;
                            break;

                        case 0x176:
                            item.tburst = val;
                            item.other[(int)key] = val;
                            break;

                        case 0x177:
                            item.tfullauto = val;
                            item.other[(int)key] = val;
                            break;

                        case 0x195:
                            item.school = val;
                            break;

                        case 0x197:
                            item.nanocost = val;
                            break;

                        case 0x1a4:
                            item.atype = val;
                            break;

                        case 0x1b4:
                            item.dtype = val;
                            break;

                        case 0x1b8:
                            item.initskill = val;
                            item.other[(int)key] = val;
                            break;

                        case 0x15:
                            if (val == 0x21) {
                                ushort lname, linfo;
                                lname = PopUShort();
                                linfo = PopUShort();
                                try {
                                    item.name = PopString(lname);
                                }
                                catch (Exception ex) {
                                    
                                    logger.FatalFormat("Could not extract item name for item aoid:{0}", item.aoid);
                                    logger.FatalFormat(ex.Message);
                                    logger.FatalFormat(ex.StackTrace);
                                    
                                }
                                try {
                                    item.description = PopString(linfo);
                                }
                                catch (Exception ex) {
                                    
                                    logger.FatalFormat("Could not extract item description for item aoid:{0}", item.aoid);
                                    logger.FatalFormat(ex.Message);
                                    logger.FatalFormat(ex.StackTrace);
                                    
                                }

                                head = false;
                            }
                            else {
                                item.other[(int)key] = val;
                            }
                            break;

                        default:
                            item.other[(int)key] = val;
                            break;
                    }

                } // head
                #endregion

                #region DUNNO
                else {
                    key = PopUInteger();
                    switch (key) {
                        case 0x0:
                            continue;

                        case 0x2: 
                            {
                                ftype = PopSInteger();
                                uint count = PopUInteger();
                                count >>= 10;

                                while (count-- > 0) {
                                    uint fkey = 0;
                                    while (fkey == 0) {
                                        if (Remaining < 4) {
                                            logger.WarnFormat("Aborting parsing on item id {0}, buffer small than expected.", item.aoid);
                                            return item;
                                        }
                                        fkey = PopUInteger();
                                    }
                                    if ((fkey & 0xcf00) != 0xcf00) {
                                        if (fkey != 0) {
                                            
                                            logger.WarnFormat("Unknown function key while parsing item {0}, key: {1}", item.aoid, fkey);
                                            logger.Warn("No further parsing performed on this item.");
                                            
                                        }
                                        return item;
                                    }
                                    else {
                                        ParseFunctions(ftype, (int)fkey & 0xFF, item);
                                    }                                

                                }
                            }
                            break;

                        case 0x4: 
                            try {
                                val = PopSInteger();
                                if (val != 0x04) {
                                    
                                    logger.WarnFormat("Error parsing item AOID:{0}, halting parse on this item. (expected 0x04 got 0x{1})", item.aoid, val.ToString("X"));
                                    
                                    return item;
                                }

                                
                                uint count = PopUInteger();
                                count >>= 10;
                                while (count-- > 0 && offset + 8 < data.Length) {
                                    Dictionary<int, int> m = new Dictionary<int, int>();
                                    uint fkey = PopUInteger();
                                    uint subcount = PopUInteger();
                                    subcount >>= 10;
                                    while (subcount-- > 0 && offset+8 < data.Length) {
                                        int subkey = PopSInteger();
                                        int subval = PopSInteger();
                                        m[subkey] = subval;
                                        
                                    }

                                    if (fkey == 0x0C) {
                                        item.attmap = m;
                                    }
                                    else if (fkey == 0x0D) {
                                        item.defmap = m;
                                    }
                                    else if (fkey == 0x03) {
                                        // ?
                                    }
                                    else {
                                        // ?
                                    }
                                }
                            }
                            catch (Exception ex) {

                                logger.WarnFormat(ex.Message);
                                logger.WarnFormat(ex.StackTrace);
                                return item;
                            }
                            break;

                        case 0x6: 
                            {
                                val = PopSInteger();
                                if (val != 0x1B) {
                                    
                                    logger.WarnFormat("Error parsing item AOID:{0}, halting parse on this item. (expected 0x1B got 0x{1})", item.aoid, val.ToString("X"));
                                    
                                    return item;
                                }

                                uint count = PopUInteger();
                                count >>= 10;
                                while (count-- > 0) {
                                    //PopSInteger(); // key
                                    //PopSInteger(); // val
                                    offset += 8;
                                }
                            }
                            break;

                        case 0xE:
                        case 0x14: 
                            try {
                                val = PopSInteger();
                                uint count = PopUInteger();
                                count >>= 10;
                                while (count-- > 0 && offset+8 < data.Length) {
                                    val = PopSInteger();
                                    uint count2 = PopUInteger();
                                    count2 >>= 10;
                                    while (count2-- > 0) {
                                        offset += 4;
                                    }
                                }
                            }
                            catch (Exception ex) {
                                logger.WarnFormat(ex.Message);
                                logger.WarnFormat(ex.StackTrace);
                                return item;
                            }
                            break;

                        case 0x16: 
                            {
                                val = PopSInteger();
                                if (val != 0x24) {
                                    
                                    logger.WarnFormat("Error parsing item AOID:{0}, halting parse on this item. (expected 0x24 got 0x{1})", item.aoid, val.ToString("X"));
                                    
                                    return item;
                                }

                                uint count = PopUInteger();
                                count >>= 10;
                                while (count-- > 0) {
                                    int rhook = PopSInteger();
                                    uint subcount = PopUInteger();
                                    subcount >>= 10;
                                    item.reqs = ParseRequirements(subcount, rhook);
                                }

                            }
                            break;


                        case 0x17:
                            return item;


                        default:
                            if ((key & 0xcf00) == 0xcf00) {
                                ParseFunctions(ftype, (int)key & 0xFF, item);
                            }
                            break;
                    }
                }
                #endregion
            } // while

            return item;
        }

        private bool ParseFunctions(int effectType, int key, AOItem item) {
            try {
                AOItemEffect effect = new AOItemEffect();
                effect.hook = effectType;
                effect.type = key;

                // Skip 4 integers
                offset += 8;

                int count = PopSInteger();
                if (count > 0) {
                    effect.requirements = ParseRequirements((uint)count, -1);
                }

                if (Remaining < 12)
                    return false;

                effect.hits = PopSInteger();
                effect.delay = PopSInteger();
                effect.target = PopSInteger();
                offset += 4;

                switch (key) {
                    case UPLOAD_NANO:
                        effect.target = 2;
                        effect.values.Add(PopSInteger());
                        break;

                    case TEXT_EFFECT: // chat text, length, text
                        effect.text = PopString(PopSInteger());
                        if (key == 0x34)
                            offset += 4 * 5;

                        break;

                    case UNKNOWN_TEXT1:
                        effect.text = PopString(PopSInteger());
                        break;

                    case TEXT_FLOAT:
                        effect.text = PopString(PopSInteger());
                        PopSInteger(); // Text display time?
                        break;

                    case UNKNOWN_TEXT2:
                        effect.text = PopString(PopSInteger());
                        PopSInteger();
                        break;

                    case UNKNOWN_TEXT3:
                        effect.text = PopString(PopSInteger());
                        offset += 8;
                        break;

                    case UNKNOWN_TEXTS1:
                        effect.text = string.Format("{0}, {1}", PopString(PopSInteger()), PopString(PopSInteger()));
                        PopSInteger();
                        break;

                    case UNKNOWN_TEXT4:
                        effect.text = PopString(PopSInteger());
                        break;

                    case UNKNOWN_TEXT5:
                        effect.text = PopString(PopSInteger());
                        PopSInteger();
                        break;

                    // These are actually floats, but just ignore that for now.
                    case DEBUG_TEXT: 
                        {
                            effect.values.Add(PopSInteger()); // x
                            effect.values.Add(PopSInteger()); // z
                            effect.values.Add(PopSInteger()); // y

                            int slen = PopSInteger();
                            if (!CanPopString(slen))
                                return false;

                            effect.text = PopString(slen);
                        }
                        break;

                    case EVENT_TEXT:
                        {
                            int slen = PopSInteger();
                            if (!CanPopString(slen))
                                return false;
                            effect.text = PopString(slen);
                        }
                        break;

                    case UNKNOWN_TEXT_N: 
                        {
                            int slen = PopSInteger();
                            if (!CanPopString(slen))
                                return false;

                            effect.text = PopString(slen);

                            slen = PopSInteger();
                            if (!CanPopString(slen))
                                return false;
                            effect.values.Add(slen);

                        }
                        break;
                }
                // TODO:
                if (!s_effectkeys.ContainsKey((ushort)key)) {
                    return false;
                }
                else {
                    for (int i = 0; i < s_effectkeys[(ushort)key]; i++) {
                        effect.values.Add(PopSInteger());
                    }
                }

                item.effects.Add(effect);
                return true;
            }
            catch (ArgumentOutOfRangeException ex) {
                
                logger.Warn(ex.Message);
                
                return false;
            }
            catch (ArgumentException ex) {
                
                logger.Warn(ex.Message);
                
                return false;
            }
        }


        /// <summary>
        /// Parse the requirements for an item / nano
        /// </summary>
        /// <param name="data"></param>
        /// <param name="offset"></param>
        /// <param name="subcount"></param>
        /// <param name="rhook"></param>
        /// <returns></returns>
        private List<AOItemRequirements> ParseRequirements(uint subcount, int rhook) {

            List<AOItemRequirements> result = new List<AOItemRequirements>();

            int skill;
            int count;
            int op;
            int opmod = 0;
            bool haveReq = false;
            int reqid = 0;

            AOItemRequirements req = new AOItemRequirements();
            while (subcount-- > 0) {
                // Overflow
                if (offset + 12 > data.Length) {
                    return result;
                }


                skill = PopSInteger();
                count = PopSInteger();
                op = PopSInteger();

                if (skill == 0 && count == 0) {
                    if (op == 0x12) {
                        opmod |= op;
                    }
                    else {
                        req.opmod |= opmod | op;
                    }
                }
                else {
                    if (haveReq) {
                        result.Add(req);
                        req = new AOItemRequirements();
                    }
                    else {
                        haveReq = true;
                    }
                    req.id = reqid++;
                    req.type = rhook;
                    req.attribute = skill;
                    req.count = count;
                    req.op = op;
                    req.opmod = 0;
                }

            }

            if (haveReq) {
                result.Add(req);
            }

            return result;
        }
    }
}
