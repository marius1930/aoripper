﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using log4net;

namespace AORipper {
    class IOHelper {
        private static ILog logger = LogManager.GetLogger(typeof(IOHelper));
        public static uint ReadInteger(FileStream fs, bool endian = false) {
            byte[] array = new byte[4];
            if (fs.Read(array, 0, 4) != 4) {
                logger.WarnFormat("ReadInteger called with only {0} bytes remaining!", fs.Length - fs.Position);
                return 0;
            }
            else {
                if (endian && BitConverter.IsLittleEndian) {
                    Array.Reverse(array);
                }
                return BitConverter.ToUInt32(array, 0);
            }
        }


        public static ushort ReadShort(FileStream fs) {
            byte[] array = new byte[2];
            if (fs.Read(array, 0, 2) != 2) {
                logger.WarnFormat("ReadShort called with only {0} bytes remaining!", fs.Length - fs.Position);
                return 0;
            }
            else {
                return BitConverter.ToUInt16(array, 0);
            }
        }
    }
}
