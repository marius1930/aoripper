﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AORipper.Structures {
    class AOItemRequirements {
        public virtual long pk { get; set; }
        public virtual int id { get; set; }
        public virtual int type { get; set; }
        public virtual int attribute { get; set; }
        public virtual int count { get; set; }
        public virtual int op { get; set; }
        public virtual int opmod { get; set; }

        public virtual AOItem item { get; set; }
    }
}
