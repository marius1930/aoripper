﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AORipper.Structures {
    class AOItem {
        public AOItem() {
            attmap = new Dictionary<int, int>();
            defmap = new Dictionary<int, int>();
            other = new Dictionary<int, int>();
            reqs = new List<AOItemRequirements>();
            effects = new List<AOItemEffect>();
        }

        // aodb
        //unsigned public virtual int hash { get; set; }
        //char metatype { get; set; }
        public virtual long  aoid { get; set; }
        //public virtual int  patch { get; set; }
        public virtual int  flags { get; set; }
        public virtual int  props { get; set; }
        public virtual int  ql { get; set; }
        public virtual int  icon { get; set; }
        public virtual string name { get; set; }
        public virtual string description { get; set; }

        // aodb_item
        public virtual int type { get; set; }
        public virtual int slot { get; set; }
        public virtual int defslot { get; set; }
        public virtual int value { get; set; }
        public virtual int tequip { get; set; }

        // aodb_nano
        public virtual int crystal { get; set; }
        public virtual int ncu { get; set; }
        public virtual int nanocost { get; set; }
        public virtual int school { get; set; }
        public virtual int strain { get; set; }
        public virtual int duration { get; set; }

        // aodb_weapon
        public virtual int multim { get; set; }
        public virtual int multir { get; set; }
        public virtual int tburst { get; set; }
        public virtual int tfullauto { get; set; }
        public virtual int clip { get; set; }
        public virtual int dcrit { get; set; }
        public virtual int atype { get; set; }
        public virtual int mareq { get; set; }

        // both
        public virtual int tattack { get; set; }
        public virtual int trecharge { get; set; }
        public virtual int dmax { get; set; }
        public virtual int dmin { get; set; }
        public virtual int range { get; set; }
        public virtual int dtype { get; set; }
        public virtual int initskill { get; set; }
        public virtual IDictionary<int, int> attmap { get; set; }
        public virtual IDictionary<int, int> defmap { get; set; }

        // other tables
        public virtual IDictionary<int, int> other { get; set; }
        public virtual IList<AOItemRequirements> reqs { get; set; }
        public virtual IList<AOItemEffect> effects { get; set; }
    }
}
